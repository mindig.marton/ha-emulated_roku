# Deprecated

This custom_component is no longer being maintained as it has been merged into Home Assistant 0.86.0 (and will be available once released.)

There has been multiple changes to configuration, please refer to
https://www.home-assistant.io/components/emulated_roku/ for up-to-date instructions.

# emulated_roku

This component integrates the [emulated_roku](https://gitlab.com/mindig.marton/emulated_roku) library into Home Assistant,    
so remotes such as Harmony and Android apps can connect to it through WiFi as it were a Roku player.  
Home Assistant will see key presses and app launches as Events, which you can use as triggers for automations.  
This is basically the opposite of the remote.harmony component.  
Multiple Roku servers may be started if you run out of buttons by specifying multiple `listen_ports`.   
When using multiple servers, you can filter the roku USN which can be extracted from the Home Assistant logs, the roku API or Harmony when adding the device.  
NOTE: Windows is not supported because Home Assistant uses `ProactorEventLoop` which does not support UDP sockets.

configuration.yaml
```yaml
emulated_roku:
  host_ip: 192.168.1.150 # should be set to Home Assistant's address
  advertise_ip: 10.0.0.10 # optional, defaults to host_ip if not specified
  upnp_bind_multicast: True # optional, defaults to True
  listen_ports:
    - 8060
    - 8061:8080 # optional format when using something like Docker - port:advertise_port
```

automations.yaml
```yaml
- id: amp_volume_up
  alias: Increase amplifier volume
  trigger:
  - platform: event
    event_type: roku_command
    event_data:
      roku_usn: 5sEE3ioBPJU2m3ZR2QBHDb
      type: keypress
      key: Fwd
  action:
  - service: media_player.volume_up
    entity_id: media_player.amplifier
```

`event_data` contains:
* `roku_usn` the serial number of the roku. Not required for when using one Emulated Roku instance.
* `type`
    * keypress
    * keyup
    * keydown
    * launch

Depending on `type`, `event_data` may contain additional keys:
* keypress / keyup / keydown
    * `key` the code of the pressed key
* launch
    * `app_id` the id of the app that was launched. Between 1-10. (The Emulated API reports 10 dummy apps)

After starting up, you can check if the Emulated Roku is reachable at the specified ports on your hass instance (eg.: `http://192.168.1.101:8060/` )  
Make note of the value between the serialNumber tags (`<serialNumber>5sEE3ioBPJU2m3ZR2QBHDb</serialNumber>`)  
You'll need to set `roku_usn` to this value in the automation trigger if you have set up multiple Emulated Rokus.  
If you change your advertised IP or ports then the serial number will change and you will have to re-add your Roku in Harmony.

The available keys are listed here:
https://sdkdocs.roku.com/display/sdkdoc/External+Control+API#ExternalControlAPI-KeypressKeyValues

Tested with Logitech Harmony and some Android remotes.  

Known limitations:
* Some Android remotes send keyup / keydown events instead of keypress.
* Functionality other than key presses and app launches are not implemented yet.
* Harmony cannot launch apps as it uses IR instead of the WiFi API